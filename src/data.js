export default {
  "configuration": {
    "title": "Appliance Configuration",
    "lastUpdated": "Last Updated",
    "lastUpdatedTimestampWithUsername": "{0} by {1}",
    "warning": "Warning!",
    "confirmDialog": {
      "title": "Apply Configuration Changes to Appliance",
      "description": "While the system applies changes, you cannot make any additional modifications. If a reboot is required, the appliance will go offline until the process has finished.",
      "widgetListTitle": "The following configurations have changed",
      "applianceReboot": "Appliance will be rebooted"
    },
    "navigateAwayDialog": {
      "title": "Apply Configuration Changes to Appliance",
      "content1": "You are attempting to navigate away from this appliance's configuration settings. Doing so will result in the loss of any changes made in the configuration items.",
      "content2": "Do you wish to continue to navigate away and lose all of your changes or would you like to remain on the configuration pages and continue editing?"
    },
    "discardChangesDialog": {
      "title": "Discard Changes",
      "content": "Are you sure you want to cancel? This will delete your CSR, and you will have to regenerate a new CSR and obtain a new certificate."
    },
    "replaceIdentityDialog": {
      "title": "Replace Identity",
      "content": "Are you sure you're ready to replace this appliance identity? <br />Confirm you've added the certificate(s) to the required Trust Stores before you replace your SSL/TLS Appliance Identity. For important instructions, please refer to  <a target=\"_blank\" href=\"/smc/help/enterprise/help_enterprise.htm#cshid=62005\">Stealthwatch Help</a>."
    },
    "dialogButtons": {
      "navigateAway": "Navigate Away",
      "continueEditing": "Continue Editing",
      "cancel": "Cancel",
      "applyChanges": "Apply Changes",
      "discardChanges": "Discard Changes",
      "replaceIdentity": "Replace Identity"
    },
    "pageButtons": {
      "applySettings": "Apply Settings",
      "cancel": "Cancel",
      "configurationmenu": "Configuration Menu",
      "add": "Add",
      "addNew": "Add New",
      "edit": "Edit",
      "delete": "Delete",
      "update": "Update",
      "applyAuthentication": "Apply Authentication",
      "save": "Save"
    },
    "tabTitles": {
      "appliance": "Appliance",
      "networkServices": "Network Services",
      "general": "General"
    },
    "banner": {
      "failureMessage": {
        "serviceFailure": "Service Failed",
        "applyConfigFailure": "We couldn't save your configuration changes. Review the errors on each tab and try again.",
        "saveConfigFailure": "We couldn't save your configuration changes. Please revert your changes, click Apply Settings, and then try again.",
        "validationFailure": "Your configuration changes are invalid for {0}. {1}",
        "outdatedConfigFailure": "We couldn�t save your changes because another configuration change is in progress. Click <a href=\"#!/inventory\">OK</a> to return to Inventory, and then edit your appliance configuration again."

      },
      "successMessage": "Configuration changes saved successfully.",
      "warningMessage": "Attention: In order to add a new appliance either all of the default certificate files need to be in place already on both the appliances and the managing SMC, or if custom certificates are used for the managing SMC the corresponding SSL clients certificates must already be installed on the individual appliance being added.  For more information see the <a target=\"_blank\" href=\"{0}\">appliance certificates page</a>"
    },
    "widgetContainer": {
      "modified": "Modified",
      "revert": "Revert",
      "reboot": "Modification Requires Reboot",
      "defaultBannerErrorMessage": "Correct your configuration settings and try again. For more information, please refer to <a target=\"_blank\" href=\"/smc/help/enterprise/help_enterprise.htm#cshid=62000\">Stealthwatch Help</a>."
    },
    "adminUIPanel": {
      "title": "More Configuration Options",
      "message": "For more appliance configuration options, log in to the <a target=\"_blank\" href=\"{0}\">Appliance Administration</a> interface."
    },
    "widgets": {
      "aide": {
        "title": "Advanced Intrusion Detection Environment",
        "fields": {
          "enableAIDE": "Enable AIDE"
        }
      },
      "ldapSetup": {
        "title": "LDAP Setup",
        "addLdap": "Add LDAP",
        "certRevocationInfo": "Select the response LDAP users will receive at login based on the LDAP server revocation status.",
        "certRevocationInfoDisabled": "Disabled: Do not check the LDAP server revocation status. Always allow users to log in.",
        "certRevocationInfoSoftFail": "Soft Fail: Users can log in to the system if the LDAP certificate revocation status is unavailable.",
        "certRevocationInfoHardFail": "Hard Fail: Users cannot log in to the system if the LDAP certificate revocation status is unavailable.",
        "bindUserInfo": "Enter the value using the 'Bind user' Example: CN=admin,OU=users,DC=example,DC=com",
        "baseAccountsInfo": "Enter the value using the 'Base Accounts' Example: DC=example,DC=com",
        "actionsMenu":{
          "delete": "Delete",
          "edit": "Edit"
        },
        "labels": {
          "friendlyName": "Friendly Name",
          "description": "Description",
          "serverAddress": "Server Address",
          "port": "Port",
          "certRevocation": "Cert Revocation",
          "certificateRevocation": "Certificate Revocation",
          "bindUser": "Bind User",
          "bindPassword": "Bind Password",
          "password": "Password",
          "confirmPassword": "Confirm Password",
          "baseAccounts": "Base Accounts",
          "action": "Actions"
        },
        "certRevocationOptions": {
          "disabled": "Disabled",
          "softFail": "Soft Fail",
          "hardFail": "Hard Fail"
        }
      },
      "host": {
        "title": "Host Naming",
        "fields": {
          "hostName": "Host Name",
          "domainName": "Domain Name"
        }
      },
      "auditLogDestination": {
        "title": "Audit Log Destination (Syslog over TLS)",
        "warningMessage": "Add your Syslog SSL/TLS certificate to this appliance's Trust Store before you configure the Audit Log Destination.",
        "certRevocationInfo": "Select how we will check the Syslog server revocation status during the TLS connection.",
        "certRevocationInfoDisabled": "Disabled: Do not check the Syslog server revocation status. Always allow the TLS connection.",
        "certRevocationInfoSoftFail": "Soft Fail: Connect Syslog over TLS if the Syslog server revocation status is unavailable.",
        "certRevocationInfoHardFail": "Hard Fail: Do not connect Syslog over TLS if the Syslog server revocation status is unavailable.",
        "fields": {
          "ipAddress": "Ip Address",
          "portNumber": "Destination Port (Default 6514)",
          "certificateRevocation": "Certificate Revocation"
        }
      },
      "openingMessage": {
        "title": "Opening Message",
        "fields": {
          "message": "Enter the text that should appear to all users who try to login to this appliance",
          "charMax": "10,000 char max"
        }
      },
      "networkInterfaces": {
        "title": "Network Interfaces",
        "labels": {
          "name": "Name",
          "ipvAddress": "Ipv4 Address",
          "subnetMask": "Subnet Mask",
          "defaultGateway": "Default Gateway",
          "broadcast": "Broadcast",
          "actions": "Actions",
          "edit": "Edit",
          "settings": "Settings",
          "cancel": "Cancel",
          "save": "Save"
        },
        "fields": {
          "ipv4Address": "Ipv4 Address",
          "subnetMask": "Subnet Mask",
          "defaultGateway": "Default Gateway",
          "broadcast": "Broadcast"
        }
      },
      "ntp": {
        "title": "NTP Server",
        "labels": {
          "dropdownPlaceholder": "Select a server",
          "addNew": "Add New",
          "ipDuplicationError": "This Server Name already exist.",
          "selectServerDropdown": "NTP Servers",
          "serverName": "SERVER NAME OR IP ADDRESS",
          "actions": "ACTIONS",
          "authKey": "Key Value",
          "authId": "Key ID",
          "authenticateConnection": "Authenticate Connection",
          "delete": "Delete",
          "addServer": "Add an NTP Server",
          "ntpServerRequired": "Add at least 1 NTP server to your configuration.",
          "addCustom": "Add Custom Server",
          "isCustomEmptyError": "Enter a server name or IP address.",
          "hostNameResolutionError": "Enter a valid IP address or server name. If you add a server name, configure DNS Server and/or Local Resolution.",
          "manualConfiguration": "NTP communication is currently disabled. Contact your system administrator to manually configure the system time.",
          "manualConfigurationError": "We couldn't save your configuration changes because NTP communication is currently disabled. Contact your system administrator to manually configure the system time.",
          "ntpServersValidationError": "We couldn't validate server: {0}. Delete the NTP server and add it again.",
          "ntpInfoIconTooltip": {
            "selectOrAdd": "Select an NTP server or add a custom server to your appliance configuration. If you add a custom server, configure DNS Server and/or Local Resolution.",
            "auditLogs": "To keep the time up-to-date, the system checks your NTP servers periodically. For details, review Appliance Support > Audit Logs.",
            "authentication": "If you add authentication, go to Appliance Support > Audit Logs to confirm the NTP server communication status and system time changes are successful. If authentication failed, you do not have a connection with that server. Delete the NTP server and add it again."
          }
        }
      },
      "passwordPolicy": {
        "title": "Password Policy",
        "fields": {
          "general": "General",
          "passwordCondition": "Password must be at least",
          "passwordExpires": "Password expires after",
          "charactersLong": "characters long",
          "days": "days",
          "passwordComplexity": "Password Complexity",
          "passwordMinimumCondition": "Password must contain a minimum of",
          "upperCase": "upper case letters",
          "lowerCase": "lower case letters",
          "numbers": "numbers",
          "symbols": "symbols",
          "numberOfPreviousPasswords": "Number of previous passwords disallowed",
          "passwords": "passwords",
          "minDaysBetweenChanges": "Minimum number of days allowed between password changes",
          "passwordChangeCharsDiff": "Number of characters different from previous password",
          "characters": "characters"

        },
        "invalidPasswordComplexity": "The sum of all fields in Password Complexity should not exceed 30 characters"
      },
      "securityLockout": {
        "title": "Security Lockouts",
        "fields": {
          "unsuccessfulAttempts": "Number of unsuccessful attempts",
          "durationOfLockout": "Duration of lockout",
          "maxConcurrentUserSessions": "Maximum number of concurrent sessions",
          "numbers": "numbers",
          "minutes": "minutes"
        }
      },
      "sso": {
        "title": "Single Sign-On Authentication",
        "helpText": "Use System Configuration to configure SSO. For instructions, please refer to the <a target=\"_blank\" href=\"{0}\">Stealthwatch Installation and Configuration Guide</a>."
      },
      "snmpAgent": {
        "title": "SNMP Agent",
        "fields": {
          "community": "Read only community",
          "port": "SNMP port (default 161)",
          "sysLocation": "sysLocation",
          "sysContact": "sysContact",
          "sysName": "sysName",
          "sysServices": "sysServices",
          "sysDescr": "sysDescription",
          "sysObjectId": "sysObjectId",
          "snmpVersion": "SNMP Version",
          "username": "User Name",
          "encryptionPass": "encryption password",
          "authPass": "authentication password"
        }
      },
      "dodin": {
        "title": "DoDIN Notifications",
        "labels": {
          "toEmail": "To Email",
          "toEmailInfoMessage": "Send secure compliance notifications to this email address."
        },
        "error": {
          "requiresSmtpConfiguration": "SMTP configuration is required to enable DODIN Notifications."
        }
      },
      "smtp": {
        "title": "SMTP Configuration",
        "infoTooltip": "To set up secure email compliance notifications, configure SMTP Configuration and DoDIN Notifications.",
        "labels": {
          "fromEmail": "From Email",
          "smtpServer": "SMTP Server",
          "port": "Port",
          "userName": "User Name",
          "password": "Password",
          "encryptionType": "Encryption Type",
          "smtps": "SMTPS",
          "starttls": "STARTTLS",
          "unencrypted": "Un-encrypted"
        },
        "invalidDomainNameOrIpAddress": "Enter a fully qualified domain name or IPv4 address."
      },
      "localResolution": {
        "title": "Local Resolution",
        "ipDuplicationError": "This IP Address already exists.",
        "labels": {
          "hostName": "HOST NAME",
          "hostIPAddress": "HOST IP ADDRESS",
          "addNewHost": "Add New Host",
          "actions": "ACTIONS"
        },
        "fields": {
          "hostName": "HOST NAME",
          "hostIPAddress": "HOST IP ADDRESS"
        }
      },
      "sessionTimeout": {
        "title": "Protected Sessions Time-Out",
        "fields": {
          "timeoutSecondsPrefix": "Request user re-authentication for administrator-only functions after",
          "timeoutSecondsSuffix": "minutes",
          "idleTimeoutPrefix": "Log out user due to inactivity after",
          "idleTimeoutSuffix": "minutes",
          "aboutUsingZero": "If you enter 0, the user will never be logged out."
        }
      },
      "ssh": {
        "title": "SSH",
        "fields": {
          "enabled": "Enable SSH",
          "rootAccessEnabled": "Enable Root SSH Access"
        }
      },
      "dns": {
        "title": "DNS Server",
        "addDnsTitle": "Add DNS Server",
        "ipDuplicationError": "This IP Address already exists.",
        "fields": {
          "ipAddress": "IP Address",
          "timeoutValue": "Global DNS Timeout Value (in seconds, max = 30)"
        },
        "list": {
          "ipAddress": "DNS Server IP Address",
          "actions": "Actions"
        }
      },
      "fips": {
        "title": "Compliance Mode",
        "warningMessage": "Before you enable a compliance mode, follow the instructions in <a target=\"_blank\" href=\"{0}\">Stealthwatch Help</a>. The procedures include installing certificates and confirming all passwords are compliant. If you enable one of these compliance modes, make sure you enable the same mode on all of your appliances.",
        "fields": {
          "enableFips": "Enable FIPS Encryption Libraries",
          "enableCC": "Enable Common Criteria Encryption Libraries",
          "unlock": "Unlock",
          "addCertToTrustStore": "I have added the correct compliant certificates to the Trust Stores.",
          "replaceIdentityCert": "I have replaced the identity certificate on every appliance in my cluster."
        },
        "fipsFormTitle": "Compliance Mode Prerequisites",
        "confirmationNote": "I confirm that I have completed the required steps, and I understand that improperly enabling FIPS or Common Criteria encryption libraries can stop Stealthwatch appliance communications and cause data loss.",
        "confirmationLabel": "Type \"12358\" to continue"
      },
      "configBackup": {
        "title": "Backup Configuration Encryption",
        "infoEnabledEncryption": "Enable this setting to encrypt your backup configuration files and require a password to decrypt and restore them.",
        "warningNote": "Make sure you save your password. If you lose your password, you can change it. However, you will not be able to restore a backup configuration file without the password.",
        "enableEncryptBackup":"Enable Encryption",
        "labels": {
          "password": "Backup Configuration Password",
          "confirmPassword": "Confirm Password"
        }
      },
      "dnsCache": {
        "title": "DNS Cache",
        "fields": {
          "cacheSizeInMemory": "Cache Size In Memory (Maximum of 1024 MB)",
          "mb": "MB"
        }
      },
      "externalServices": {
        "title": "External Services",
        "warningMessage": "If you enable one of these options, make sure you enable the same option on all of your Flow Collectors and Stealthwatch Management Consoles.",
        "fields": {
          "enableGA": "Enable Google Analytics",
          "enableCTA": "Enable Cognitive Analytics",
          "enableAHC": "Enable Stealthwatch Cloud Early Access",
          "enableCSM": "Enable Customer Success Metrics",
          "automaticUpdates": "Automatic Updates"
        },
        "ahc": {
          "SEULATitle": "Enabling Stealthwatch Cloud - Early Access",
          "SEULAContent": "Please read the Supplemental End User License Agreement for Stealthwatch Enterprise (SEULA) available at <a href='https://www.cisco.com/c/en/us/about/legal/cloud-and-software/software-terms.html' target='_blank'>Supplemental End-User License Agreement</a>. If you do not agree to the SEULA terms, do not enable Stealthwatch Cloud - Early Access. Please contact <a href='mailto:swc-early-access@cisco.com'>swc-early-access@cisco.com</a> to complete your configuration to gain access to analytics in development.",
          "SEULAAction": "I agree"
        },
        "csm": {
          "SEULATitle": "Enabling Customer Success Metrics",
          "SEULAContent": "Please read the Supplemental End User License Agreement for Stealthwatch Enterprise (SEULA) available at <a href='https://www.cisco.com/c/en/us/about/legal/cloud-and-software/software-terms.html' target='_blank'>Supplemental End-User License Agreement</a>. If you do not agree to the SEULA terms do not enable Customer Success Metrics.",
          "SEULAAction": "I agree"
        },
        "enableCTAInfoOne": "Enable this setting to provide additional information on the Security Insight Dashboard provided by Cognitive Analytics.",
        "enableCTAInfoTwo": "When enabled for the first time, a new cloud account is created and single-sign-on from the management console is configured.",
        "enableCTAInfoThree": "Any user with access to the management console will have access to the Cognitive Analytics Dashboard.",
        "enableCTAInfoFour": "Note: sFlow collectors do not provide the level of detail necessary for Cognitive Analytics Engines and should not be configured to send data.",
        "enableAHCInfo": "Enable Stealthwatch Cloud - Early Access to gain access to analytics in development. Contact <a href='mailto:swc-early-access@cisco.com'>swc-early-access@cisco.com</a> for more information.",
        "enableCSMInfoOne": "Customer Success Metrics (CSM) enables Stealthwatch system data to be sent to the cloud so that Customer Success can access vital information regarding the deployment, health, performance, and usage of your system.",
        "automaticUpdatesInfoOne": "Enable this setting to receive automatic software updates from Cognitive Analytics and Stealthwatch Cloud Early Access",
        "automaticUpdatesInfoTwo": "The update packages are signed, downloaded through a secure channel, and verified before installation.",
        "enableGAInfoOne": "Google Analytics telemetry collection allows collection of fully anonymised data describing performance and usage statistics of Stealthwatch."
      },
      "internetProxy": {
        "title": "Internet Proxy",
        "confirmDNSBanner": "Confirm your DNS server is configured.",
        "bannerMessage": "Enable and configure these settings if using a proxy server for Stealthwatch internet connections. Stealthwatch must communicate over the internet via HTTPS for the Stealthwatch Labs Intelligence Center (SLIC Feed), Cognitive Analytics and licensing services.",
        "proxySetup": "Proxy Setup",
        "proxyLogin": "Proxy Login Credentials (if applicable)",
        "requiredField": "This is a required field",
        "labels": {
          "ipAddress": "IP Address",
          "port": "Port",
          "userName": "User Name",
          "password": "Password",
          "domain": "Domain",
          "confirmPassword": "Confirm Password",
          "authenticationType": "Authentication Type",
          "basic": "basic",
          "ntlm": "ntlm"
        }
      },
      "tlsApplianceIdentity": {
        "title": "SSL/TLS Appliance Identity",
        "updateIdentity": "Update Identity",
        "replaceSslTls": "Replace SSL/TLS Appliance Identity",
        "replaceIdentity": "Replace Identity",
        "warningMessage": "Improperly modifying your Certificates can break your Stealthwatch System.",
        "certificateInstructions": "Your certificates are critical for your system's security. Improperly modifying your certificates can break your Stealthwatch system. Follow the instructions in <a target=\"_blank\" href=\"/smc/help/enterprise/help_enterprise.htm#cshid=62005\">Stealthwatch Help</a> to update the {0}.",
        "infoIconMessage": "The appliance identity is used for communication between Stealthwatch appliances.",
        "labels": {
          "friendlyName": "Friendly Name",
          "issuedTo": "Issued To",
          "issuedBy": "Issued By",
          "validFrom": "Valid From",
          "validTo": "Valid To",
          "serialNumber": "Serial Number",
          "keyLength": "Key Length",
          "bits": "bits",
          "rsaKeyLength": "RSA Key Length",
          "commonName": "Common Name",
          "organization": "Organization",
          "organizationalUnit": "Organizational Unit",
          "city": "Locality or City",
          "stateCountryRegion": "State or Province",
          "countryCode": "Country Code",
          "emailAddress": "Email Address",
          "certificateFile": "Certificate File",
          "certificateChainFile": "Certificate Chain File",
          "bundlePassword": "Bundle Password",
          "confirmPassword": "Confirm Password"
        },
        "error": {
          "onlyTwoAlphaBetsAllowed": "Only 2 alphabetic characters are allowed",
          "graveAccentHashDollarNotAllowed": "The following characters are not allowed: ` # $",
          "csrGenerationFailed": "Your CSR generation has failed. Please cancel and start over.",
          "friendlyNameError": "Use only letters, numbers, dashes, underscores, or periods",
          "generateCsrFailed": "Generate CSR has failed.",
          "replaceIdentityFailed": "We couldn't add the certificate to the Trust Store. Make sure the certificate is valid.",
          "checkCsrStatusFailed": "Check CSR status has failed.",
          "downloadCsrFailed": "Download CSR has failed.",
          "cancelFailed": "Cancel process has failed."
        },
        "identityWorkflowSelectorTitle": "Do you need to generate a CSR?",
        "generateACsr": "Generate a CSR",
        "generatingCsr": "Generating CSR",
        "csrInProgressStatus": "In Progress",
        "csrFailedStatus": "Failed",
        "generateCsr": "Generate CSR",
        "downloadCsr": "Download CSR",
        "chooseFile": "Choose File",
        "cancel": "Cancel"
      },
      "trustStore": {
        "title": "Trust Store",
        "addCaCertificate": "Add Certification Authority Certificate",
        "labels": {
          "useStrictCertificate": "Enable Strict Certificate Management"
        },
        "addCertificate": "Add Certificate",
        "friendlyNameAlreadyExists": "This name is already used. Enter a unique name",
        "strictCertificateInfoHeader": "When you enable strict certificate management:",
        "trustListedCertificates": "The appliance trusts only the certificates saved to the Trust Store. It does not trust default system certificates.",
        "trustLocalCertificates": "The SMC Desktop Client trusts only certificates saved on your local computer.",
        "strictCertificateAdditionalInfo1": "For more information, please refer to",
        "strictCertificateAdditionalInfo2": "Stealthwatch Help",
        "error": {
          "friendlyNameError": "Use only letters, numbers, dashes, underscores, or periods"
        }
      },
      "tlsClientIdentities": {
        "title": "Additional SSL/TLS Client Identities",
        "addSslClientIdentity": "Add SSL/TLS Client Identity",
        "addClientIdentity": "Add Client Identity",
        "infoIconMessage": "The client identity is used for communication between external services such as Cisco Identity Services Engine (ISE) and Cisco Security Packet Analyzer."
      },
      "validationMessage": {
        "invalidHostName": "Provided Host Name is invalid. Should be alphanumeric and -.",
        "invalidHostNameLength": "Host Name should not exceed 64 characters.",
        "hostNameNullOrEmpty": "Host Name configuration is null.",
        "invalidDomainName": "Provided Domain Name is invalid. Should be alphanumeric and -.",
        "invalidDomainNameLength": "Domain Name should not exceed 255 characters.",
        "invalidMessage": "Opening Message should not contain <, >, and '.",
        "invalidMessageLength": "Opening Message should not exceed 10000 characters.",
        "invalidIpAddress": "Provided IP Address is invalid.",
        "invalidSubnetMask": "Provided Subnet Mask is invalid.",
        "invalidDefaultGateway": "Provided Default Gateway is invalid.",
        "invalidBroadcast": "Provided Broadcast is invalid.",
        "nameNullOrEmpty": "Interface Name should not be null.",
        "invalidNameLength": "Interface Name should not exceed 256 characters.",
        "nameNotMatch": "Provided Network Interface configuration doesn't match with existing configuration.",
        "emptyNtp": "NTP Server list should not be empty.",
        "duplicateNtp": "Duplicate NTP Server configuration provided.",
        "invalidConfig": "Provided NTP Server configuration is un-resolvable.",
        "invalidMinLength": "Provided Minimum length is invalid. Minimum = 8 and Maximum = 30.",
        "invalidExpirationDays": "Provided Expiration Date is invalid. Should not be greater than 365.",
        "invalidMinUppercase": "Minimum Upper Case criteria invalid. Should not be greater than 30.",
        "invalidMinLowercase": "Minimum Lower Case criteria invalid. Should not be greater than 30.",
        "invalidMinNumbers": "Minimum Numbers criteria invalid. Should not be greater than 30.",
        "invalidMinSymbols": "Minimum Symbols criteria invalid. Should not be greater than 30.",
        "snmpAgentNull": "Provided SNMP Configuration is null.",
        "invalidSysServices": "Provided SysServices is invalid. Minimum = 0 and Maximum = 126.",
        "invalidSysObjectId": "Provided SysObjectID is invalid. Should be alphanumeric.",
        "invalidAgentCommunity": "Provided Agent Community is invalid.",
        "invalidSysDescr": "Provided SysDescr is invalid.",
        "invalidSysLocation": "Provided SysLocation is invalid.",
        "invalidSysContact": "Provided SysContact is invalid.",
        "invalidSysName": "Provided SysName is invalid.",
        "invalidSessionTimeout": "Invalid Session Timeout, only 0 and minutes in the range of 2 to 1440 is allowed.",
        "sshNull": "Provided SSH configuration is null",
        "rootAccessEnabled": "Root Access cannot be enabled without enabling SSH.",
        "invalidTimeoutValue": "Invalid DNS Timeout value. Minimum =  1, Maximum = 30.",
        "invalidIpAddressLength": "Provided IP Address length is invalid.",
        "ipAddressNullOrEmpty": "Provided IP Address is empty or null",
        "invalidPort": "Provided port is invalid.",
        "ntlmAndFipsIncompatible": "Disable FIPS encryption libraries to use NTLM authentication. For more information, please refer to Stealthwatch Help.",
        "invalidType": "Type can only be either basic or ntlm.",
        "invalidDomain": "Domain should be alphanumeric and should not exceed 255 characters.",
        "invalidDNSCache": "Invalid DNS Cache size, Minimum = 0, Maximum = 1024.",
        "externalServicesNull": "External services configuration is null.",
        "autoUpdateEnabled": "Auto Update cannot be enabled without enabling Cognitive Threat Analytics or Host Classifier.",
        "applianceIdentityNull": "Provided Appliance Identity Configuration is null.",
        "identityCertificateNull": "No Identity Certificate Provided.",
        "invalidIdentityCertificate": "Uploaded Identity Certificate is either expired or not valid.",
        "invalidChainCertificate": "Uploaded Chain Certificate is either expired or not valid.",
        "invalidRsaKeylength": "We couldn�t validate the certificate. Make sure the RSA key length is 2048 bits or more.",
        "csrNotFound": "Generated CSR file is not found.",
        "csrNotMatch": "The certificate details do not match the CSR. Upload a valid certificate from the certificate provider.",
        "clientIdentityNull": "Provided Client Identity Configuration is null.",
        "moreThanOneCertificate": "Cannot upload more than one certificate at the same time.",
        "certificatePresentOnStrict": "There should be at least one certificate present when Strict Certificate Management is enabled.",
        "missingIdentityCertificate": "We couldn�t delete this certificate because it is required for your appliance identity. For more information, please refer to <a href=\"/smc/help/enterprise/en-us/Content/Central_Manager/CM_Trust_Store.htm\">Stealthwatch Help</a>.",
        "failedIdentityInteraction": "We couldn't decode the content of a certificate in the Trust Store. For more information, please refer to <a href=\"/smc/help/enterprise/en-us/Content/Central_Manager/CM_Trust_Store.htm\">Stealthwatch Help</a>.",
        "duplicateLdapName": "This name already exists. Enter a unique name.",
        "invalidBindUser": "The value you entered is not a valid 'Bind User'. Example: CN=admin,OU=users,DC=example,DC=com",
        "invalidBaseAccounts": "The value you entered is not a valid  'Base Accounts'. Example: DC=example,DC=com",
        "invalidSmtpServer": "Provided SMTP server address is invalid.",
        "invalidUserName": "Provided username is invalid.",
        "usernameNullOrEmpty": "Provided username is empty or null.",
        "passwordNullOrEmpty": "Provided password is empty or null.",
        "invalidEmailAddress": "Provided email address is invalid.",
        "emailAddressNullOrEmpty": "Provided email address is empty or null.",
        "invalidCcEnabled": "CC requires FIPS"
      },
      "modifyMessage": "For more information, please refer to",
      "link": "here.",
      "passwordMismatch": "Make sure your password entries match.",
      "yetToBe": "Yet to be implemented",
      "stealthwatchHelp": "Stealthwatch Help."
    },
    "errorDialog": {
      "title": "Error Applying Settings",
      "okButton": "OK",
      "message": "Your configuration changes are invalid. Click each tab to review the errors and correct them."
    }
  }
}
