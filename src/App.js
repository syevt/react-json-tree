import React, {Component} from 'react';
import './App.css';
import Node from './Node';
import data from './data';

export default class App extends Component {
    state = {
        data: data
    };

    handleClick = () => {
        console.log(this.state.data);
    };

    render() {
        return (
            <div>
                <button onClick={this.handleClick}>Click</button>
                <Node content={this.state.data}/>
            </div>
        );
    }
}
