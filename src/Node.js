import React, {Component} from 'react';

export default class Node extends Component {
    state = {value: this.props.content};

    onInputChanged = (e) => {
        const {value} = e.target;

        this.setState({
            value: value
        });
    }

    onKeyClick = () => {
        console.log(this.state.value);
    }

    render() {
        const {label, content} = this.props;
        let el;

        if((typeof content) == 'string') {
            el = <li>
                    <span onClick={this.onKeyClick}>{label}</span>
                    <input value={this.state.value} onChange={this.onInputChanged}/>
                </li>;
        } else {
            const children = Object.entries(content).map((entry) => {
                return (
                    <Node key={entry[0]} label={entry[0]} content={entry[1]}/>
                );
            });
            el = <><li>{label}</li><ul>{children}</ul></>;
        }

        return <div>{el}</div>;
    }
}
